# dVRK Organ Dataset Acquisition Tools

The Ex-Vivo dataset is a, soon to be released under permissive licence (most likely CC-BY-4, still under discussion), dataset acquired at Politecnico di Milano. The dataset contains colour, infra red and depth images of biological tissues (veal kidney, liver and fat) using a daVinci surgical system endoscope (576x720px frame, 30Hz, Intuitive surgical, USA) and an Intel Realsense D435 camera (Intel corporation, USA).

This repository contains the tools used to capture this dataset. The final location of the dataset is still to be determined. A sample of dataset of images from the dataset is available in the `/example/` folder. 

The dataset contains:

- `dvrk-organ-series-1` Organ positions #1 (231 image acquisitions from different angles and distances) 
- `dvrk_organ-series-2` Organ positions #2 (270 image acquisitions from different angles and distances)
- `dvrk-organ-series-3` Organ positions #3 (270 image acquisitions from different angles and distances)
- `dvrk-organ-series-3` Organ positions #3 with daVinci tools in the scene (270 image acquisitions from different angles and distances)

The dataset also contains an additional series of images to evaluate deformable pointcloud registration:  

- `dvrk-organ-series-deformable-pre` Tissue configuration #3 (36 image acquisitions from different angles and distances)
- `dvrk-organ-series-deformable-post` Tissue configuration #3 with fixed deformation (36 image acquisitions from different angles and distances (same as "dvrk-organ-series-deformable-pre" ))

The deformation was applied to the kidney by fastening zip-tie around the centre. 

Each series contains: 

- images from the daVinci endoscope cameras (unrectified left & right images),
- intrinsic parameters for the left & right endoscope cameras in an OpenCV & ROS friendly format,
- IR (rectified) & colour (unrectified) images from a realsense D435 depth camera. The laser emitter was turned off for these images,
- depth maps registered to each camera frame and a point cloud of the scene.

| ![A sample of the images contained in the dataset](example/dataset_overview.png) |
|:--------------------------------------------------------------------------------:|
| _A sample of the images in the dataset showing the left and right endoscope images (left), the depth maps from the Realsense camera  (centre) and the colour image from the Realsense camera (rght)._ |

The biggest problem faced when recording this dataset is the different illumination for both of the cameras. The amount of light necessary to correctly illuminate the endoscope cameras creates the image acquisitions problem from IR sensors of the realsense camera.

In addition, the realsense camera has a minimum distance to correctly reconstruct the point cloud of approximately 200 mm, this forced us to put the camera further away from the target than we would have liked.

To work around these issues we performed the acquisition in 2 distinct phases using a pre-recorded camera motion:

1. Record the endoscope images with strong illumination (Intuitive Surgical xenon fiber-optic light source).
2. Record the depth maps with the IR laser ON and the infra-red & colour images with the IR laser OFF and moderate illumination.

# Experimental SetUp

| ![A view of the experimental setup for the acquisition](example/experimental_setup.png) |
|:---------------------------------------------------------------------------------------:|
| _A view of the experimental setup for the acquisition. The Intel Realsense camera is mounted to the endoscope arm with a 3D printed support._ |

# Pre-requisites 

To run the acquisition you need: 

- a dVRK system with the JSON-console running and publishing to ROS
- images from the da Vinci endoscope published to ROS
- an intel realsense D435 RGBD camera

# Install Instructions

The nodes depend on `pcl`, `dvrk_common` and `dvrk_camera_calibration` and the `realsense2_camera` driver. All the dependencies can be installed with a combination of `rosdep` and `wstool`.

Install dependencies from source:

    ~ cd catkin_ws 
    ~ wstool init 
    ~ wstool merge src/dvrk_ex_vivo_organ_dataset_tools/.rosinstall
    ~ wstool update

Install pre-compiled libraries:

    ~ rosdep install --from-path src/ --ignore-packages-from-source  

Build everything: 

    ~ catkin build

# Conceptual Overview

The `ecm_controller.py` script moves the camera arm along a pre-recorded trajectory. Once a motion is completed the script sends a message on the `~motion_complete` topic and waits for an answer on the `~acquisition_complete` topic before moving to the next position. 

The `dvrk_ex_vivo_dataset_tool` saves the images. When it receives a message on the `motion_completed` topic it starts saving the images. The actual images it saves depend on the `~acquisition_type` parameter. If it is saving the realsense images it will save the depth maps first then disable the laser, save the IR images and turn the laser back on. 

# Launch Instructions

To perform the pre-recorded motion we need to have the dVRK (daVinci Research Kit) console running with at least the ECM powered on. Launch this script by running:

    ~ rosrun dvrk_ex_vivo_organ_dataset_tools ecm_controller.py

To actually capture images you should run: 
    
    ~ roslaunch dvrk_ex_vivo_organ_dataset_tools start.launch acquisition_type:=XXXX

The `start.launch` file accepts one parameter `acquisition_type` which should be one of:

- `endoscope`, to capture the colour images from the endoscope
- `realsense`, to capture the IR, colour and depth images from the realsense camera

During each acquisition the images will be saved into a folder named after the current date and time and the type of acquisition. For each set of images corresponding to each motion of the camera arm a new sub-folder named `acquisition_X` will be created. Once both series of acquisitions are completed you can simply merge the folders together to assemble the final dataset.
