#!/usr/bin/env python

import dvrk
import sys, math, threading
import rospy, std_msgs
import numpy
import PyKDL

# 
#                       PRESETS
#
# Fast (36 images):
#      PITCH_STEPS = 2
#      YAW_STEPS = 2
#      INSERTION_STEPS = 1
#      ROLL_STEPS = 1
#
# Slow (270 images, ~ 20 minutes runtime):
#      PITCH_STEPS = 4
#      YAW_STEPS = 5
#      INSERTION_STEPS = 2
#      ROLL_STEPS = 2

PITCH_MIN = numpy.radians(-10)
PITCH_MAX = numpy.radians(10)
PITCH_STEPS = 2 # 4 # 2

YAW_MIN   = numpy.radians(0)
YAW_MAX   = numpy.radians(15)
YAW_STEPS = 2 # 5 # 2

INSERTION_MIN = 0
INSERTION_MAX = .10
INSERTION_STEPS = 1 # 2 # 1

ROLL_MIN = numpy.radians(0)
ROLL_MAX = numpy.radians(45)
ROLL_STEPS = 1 # 2 # 1

RESUME_STEP = 0

class ECMController:

    def _on_reseume_motion(self, msg):
        self._resume_motion_event.set()

    # configuration
    def configure(self, robot_name):
        print rospy.get_caller_id(), '-> configuring dvrk_arm_test for ', robot_name
        self.arm = dvrk.arm(robot_name)

        self._resume_motion_event = threading.Event()
        self._motion_complete_pub = rospy.Publisher(
            "~motion_complete", std_msgs.msg.Empty, queue_size=1)

        rospy.Subscriber("~acquisition_complete", std_msgs.msg.Empty, self._on_reseume_motion)

    def home(self):
        print rospy.get_caller_id(), '-> starting home ...'
        
        self.arm.home()
        # if not self.arm.home():
        #     print rospy.get_caller_id(), 'Error: Unable to home robot'
        #     sys.exit(-1)

        # get current joints just to set size
        goal = numpy.copy(self.arm.get_current_joint_position())
        # go to zero position, for PSM and ECM make sure 3rd joint is past cannula

        goal.fill(0)
        goal[1] = -0.70

        print rospy.get_caller_id(), '-> moving to zero position ...'
        if not self.arm.move_joint(goal, interpolate = True):
            print rospy.get_caller_id(), 'Error: Unable to move to starting position'
            sys.exit(-1)

    def do_acquisition(self):
        print rospy.get_caller_id(), '-> starting motion ...'
        # get current position
        initial_joint_position = numpy.copy(self.arm.get_current_joint_position())
        total_steps = (PITCH_STEPS + 1) * (YAW_STEPS + 1) * (INSERTION_STEPS + 1) * (ROLL_STEPS + 1)
        current_step = 0

        for pitch_step in xrange(0, PITCH_STEPS + 1):
            pitch_angle = PITCH_MIN + ((PITCH_MAX - PITCH_MIN) / PITCH_STEPS * pitch_step)

            for yaw_step in xrange(0, YAW_STEPS + 1):
                yaw_angle = YAW_MIN + ((YAW_MAX - YAW_MIN) / YAW_STEPS * yaw_step)

                for insertion_step in xrange(0, INSERTION_STEPS + 1):
                    insertion_distance = INSERTION_MIN + \
                        ((INSERTION_MAX - INSERTION_MIN) / INSERTION_STEPS * insertion_step)

                    desired_insertion = initial_joint_position[2] + insertion_distance
                    print rospy.get_caller_id(), '-> Moving to insertion of %.2f' % desired_insertion
                    self.interpolate_insertion(desired_insertion, .025)
                                        
                    for rotation_step in xrange(0, ROLL_STEPS + 1):
                        current_step +=1
                        if current_step < RESUME_STEP:
                            continue

                        rotation_angle = ROLL_MIN + \
                            ((ROLL_MAX - ROLL_MIN) / ROLL_STEPS * rotation_step)

                        goal = numpy.copy(initial_joint_position)
                        goal[0] = initial_joint_position[0] + yaw_angle
                        goal[1] = initial_joint_position[1] + pitch_angle
                        goal[2] = initial_joint_position[2] + insertion_distance
                        goal[3] = initial_joint_position[3] + rotation_angle

                        print rospy.get_caller_id(), '-> Position %i of %i: pitch: %.2f, yaw: %.2f, insertion: %.2f, roll: %.2f' %  (
                            current_step, total_steps, numpy.rad2deg(goal[1]), numpy.rad2deg(goal[0]), goal[2], numpy.rad2deg(goal[3]))

                        if not self.arm.move_joint(goal, interpolate = True):
                            print 'Error: Move failed'
                            sys.exit(-1)
                
                        # Tell the UI that we have finished moving and that it can save the images
                        self._resume_motion_event.clear()
                        self._motion_complete_pub.publish(std_msgs.msg.Empty())

                        # Wait for the UI to confirm that the arm has been moved
                        self._resume_motion_event.wait(30)
                        if not self._resume_motion_event.isSet():
                            print 'Error: the ui never answered us!'
                            sys.exit(-1)

        # return to start position
        if not self.arm.move_joint(initial_joint_position, interpolate = True):
            print rospy.get_caller_id(), 'Error: Unable to return to initial position'
            sys.exit(-1)

        print rospy.get_caller_id(), ' <- joint goal complete'


    def interpolate_insertion(self, desired_insertion, step_height):
        initial_position = numpy.copy(self.arm.get_current_joint_position())
        initial_insertion = initial_position[2]

        insertion_amount = desired_insertion - initial_insertion
        if abs(insertion_amount) < step_height:
            return

        num_steps = int( math.floor( abs(insertion_amount) / step_height) )
        if insertion_amount < 0:
            step_height = -step_height

        for step in xrange(0, num_steps):
            # Otherwise we need to make smaller steps
            goal = numpy.copy(initial_position)
            goal[2] += step * step_height
            print rospy.get_caller_id(), '  -> interpolating insertion: desired = %.2f, current = %.2f' %  (desired_insertion, goal[2])

            if not self.arm.move_joint(goal, interpolate = True):
                print 'Error: Move failed'
                sys.exit(-1)


if __name__ == '__main__':
    try:
        rospy.init_node('ecm_controller')

        application = ECMController()
        application.configure('ECM')
        application.home()

        print ''
        print rospy.get_caller_id(), '-> Press ENTER to begin'
        raw_input()

        application.do_acquisition()

    except rospy.ROSInterruptException:
        pass
