//
// Created by tibo on 16/04/18.
//

#pragma once

#include <list>

#include <opencv2/core.hpp>
#include <sensor_msgs/Image.h>

enum class MessageType {
    Success, Info, Error
};

class CaptureUI {
public:
    CaptureUI() = default;
    
    void set_endoscope_left_image(const sensor_msgs::ImageConstPtr left);
    void set_endoscope_right_image(const sensor_msgs::ImageConstPtr right);
    
    void set_realsense_infra1_image(const sensor_msgs::ImageConstPtr infra1);
    void set_realsense_infra2_image(const sensor_msgs::ImageConstPtr infra2);
    void set_realsense_rgb_image(const sensor_msgs::ImageConstPtr rgb);
    
    void set_realsense_infra1_depth_image(const sensor_msgs::ImageConstPtr depth);
    void set_realsense_infra2_depth_image(const sensor_msgs::ImageConstPtr depth);
    void set_realsense_rgb_depth_image(const sensor_msgs::ImageConstPtr depth);
    
    void add_log(const std::string line, const MessageType type);
    
    // TODO: add depth images
    
    cv::Mat render();

public: /* Public Member Variables - UI Configuration */
    
    /// The size of the individual images in the grid
    const cv::Size desired_image_size = {640, 480};
    
    const cv::Scalar text_colour = {0, 0, 0};
    const cv::Scalar success_colour = {50, 205, 50};
    const cv::Scalar error_colour = {60, 20, 220};
    
    const cv::Scalar background_colour = {240, 241, 242};
    const cv::Scalar block_background = {225, 225, 225};
    
    const int font_face = cv::HersheyFonts::FONT_HERSHEY_DUPLEX;
    const float base_font_size = 1.0;
    
    struct {
        const unsigned int top = 15;
        const unsigned int right = 15;
        const unsigned int bottom = 15;
        const unsigned int left = 15;
    } margin;
    
    struct {
        const unsigned int top = 30;
        const unsigned int right = 30;
        const unsigned int bottom = 45;
        const unsigned int left = 30;
    } padding;
    
    const unsigned int num_log_lines = 5;
    
    const unsigned int line_height = 32;
    const unsigned int line_skip = 5;
    const unsigned int gutter_size = 15;

private:
    
    void process_image(const sensor_msgs::ImageConstPtr& src, cv::Mat& dest);
    
    cv::Rect put_text(
        const cv::Point2i& pos,
        const std::string& text,
        const cv::Scalar& colour,
        const float font_scale = 1.0
    );
    
    cv::Rect put_text_centred(
        cv::Point2i pos,
        const std::string& text,
        const cv::Scalar& colour,
        const int container_width,
        const float font_scale = 1.0
    );
    
    cv::Scalar to_colour(const MessageType type);

private:
    
    cv::Mat _ui_canvas;
    
    cv::Mat _endoscope_left_image;          // Endoscope outputs - LEFT
    cv::Mat _endoscope_right_image;         // Endoscope outputs - RIGHT
    cv::Mat _realsense_infra1_image;        // Realsense outputs - IR LEFT
    cv::Mat _realsense_infra1_depth_image;  // Realsense outputs - Depth image registered to infra1
    cv::Mat _realsense_infra2_image;        // Realsense outputs - IR RIGHT
    cv::Mat _realsense_infra2_depth_image;  // Realsense outputs - Depth image registered to infra2
    cv::Mat _realsense_rgb_image;           // Realsense outputs - RGB
    cv::Mat _realsense_rgb_depth_image;     // Realsense outputs - Depth image registered to rgb
    
    std::vector<std::string> _calibration_flags;
    
    std::vector<std::string> _usage_lines{
        " F - Toggle Full Screen",
        " S - Take sample",
        " E - Toggle IR Emitter",
        " Q - Exit"
    };
    
    std::list<std::pair<std::string, MessageType> > _log_lines;
};
