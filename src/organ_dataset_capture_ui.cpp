//
// Created by tibo on 16/04/18.
//

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include "ex_vivo_organ_dataset_capture_ui.hpp"


void CaptureUI::set_endoscope_left_image(const sensor_msgs::ImageConstPtr left) {
    process_image(left, _endoscope_left_image);
}


void CaptureUI::set_endoscope_right_image(const sensor_msgs::ImageConstPtr right) {
    process_image(right, _endoscope_right_image);
}


void CaptureUI::set_realsense_infra1_image(const sensor_msgs::ImageConstPtr infra1) {
    process_image(infra1, _realsense_infra1_image);
}


void CaptureUI::set_realsense_infra2_image(const sensor_msgs::ImageConstPtr infra2) {
    process_image(infra2, _realsense_infra2_image);
}


void CaptureUI::set_realsense_rgb_image(const sensor_msgs::ImageConstPtr rgb) {
    process_image(rgb, _realsense_rgb_image);
}


void CaptureUI::set_realsense_infra1_depth_image(const sensor_msgs::ImageConstPtr depth) {
    process_image(depth, _realsense_infra1_depth_image);
}


void CaptureUI::set_realsense_infra2_depth_image(const sensor_msgs::ImageConstPtr depth) {
    process_image(depth, _realsense_infra2_depth_image);
}


void CaptureUI::set_realsense_rgb_depth_image(const sensor_msgs::ImageConstPtr depth) {
    process_image(depth, _realsense_rgb_depth_image);
}


void CaptureUI::add_log(const std::string line, const MessageType type) {
    switch(type) {
        case MessageType::Success:
        case MessageType::Info:
            ROS_INFO("%s", line.c_str());
            break;
        
        case MessageType::Error:
            ROS_ERROR("%s", line.c_str());
            break;
    }
    
    _log_lines.emplace_back(std::move(line), type);
    while (_log_lines.size() > 5)
        _log_lines.pop_front();
}


cv::Mat CaptureUI::render() {
    
    int log_block_height = padding.top + num_log_lines * (line_height + line_skip) + padding.bottom;
    int log_block_width =
        padding.left
            + (desired_image_size.width + gutter_size + desired_image_size.width * 2)
            + padding.right;
    
    int canvas_width =
        margin.left + (desired_image_size.width + gutter_size + 3 * desired_image_size.width) + margin.right;
    
    int canvas_height =
        margin.top
            + 2 * (line_height + line_skip) + 2 * desired_image_size.height
            + gutter_size + log_block_height
            + margin.right;
    
    _ui_canvas = cv::Mat(canvas_height, canvas_width, CV_8UC3, background_colour);
    
    // TODO: properly position text
    
    // Draw the two endoscope images
    const cv::Rect endoscope_left_area(
        margin.left, margin.top + 2 * (line_skip + line_height), desired_image_size.width, desired_image_size.height);
    if (!_endoscope_left_image.empty())
        _endoscope_left_image.copyTo( _ui_canvas(endoscope_left_area) );
    else
        _ui_canvas(endoscope_left_area).setTo(cv::Scalar(0, 0, 0));
    
    cv::Point text_pos(endoscope_left_area.x, margin.top);
    put_text_centred(text_pos, "Endoscope", text_colour, endoscope_left_area.width);
    text_pos.y += line_height + line_skip;
    put_text_centred(text_pos, "Left/Right", text_colour, endoscope_left_area.width);
    
    const cv::Rect endoscope_right_area(
        endoscope_left_area.tl().x, endoscope_left_area.br().y,
        desired_image_size.width, desired_image_size.height
    );
    if (!_endoscope_right_image.empty())
        _endoscope_right_image.copyTo( _ui_canvas(endoscope_right_area) );
    else
        _ui_canvas(endoscope_right_area).setTo(cv::Scalar(0, 0, 0));
    
    // Draw the 3 realsense optical images in a row
    const cv::Rect realsense_rgb_image_area(
        endoscope_left_area.br().x + gutter_size,
        endoscope_left_area.tl().y,
        desired_image_size.width,
        desired_image_size.height
    );
    if (!_realsense_rgb_image.empty())
        _realsense_rgb_image.copyTo( _ui_canvas(realsense_rgb_image_area) );
    else
        _ui_canvas(realsense_rgb_image_area).setTo(cv::Scalar(0, 0, 0));
    
    text_pos = cv::Point(realsense_rgb_image_area.x, margin.top);
    put_text_centred(text_pos, "Intel Realsense", text_colour, desired_image_size.width * 3);
    text_pos.y += line_skip + line_height;
    put_text_centred(text_pos, "RGB", text_colour, desired_image_size.width);
    
    const cv::Rect realsense_infra1_area(
        realsense_rgb_image_area.tl().x + desired_image_size.width,
        realsense_rgb_image_area.tl().y,
        desired_image_size.width,
        desired_image_size.height
    );
    if (!_realsense_infra1_image.empty())
        _realsense_infra1_image.copyTo( _ui_canvas(realsense_infra1_area) );
    else
        _ui_canvas(realsense_infra1_area).setTo(cv::Scalar(0, 0, 0));
    
    text_pos.x += desired_image_size.width;
    put_text_centred(text_pos, "Infra 1 (Left)", text_colour, desired_image_size.width);
    
    const cv::Rect realsense_infra2_area(
        realsense_infra1_area.tl().x + desired_image_size.width,
        realsense_infra1_area.tl().y,
        desired_image_size.width,
        desired_image_size.height
    );
    if (!_realsense_infra2_image.empty())
        _realsense_infra2_image.copyTo( _ui_canvas(realsense_infra2_area) );
    else
        _ui_canvas(realsense_infra2_area).setTo(cv::Scalar(0, 0, 0));
    
    text_pos.x += desired_image_size.width;
    put_text_centred(text_pos, "Infra 2 (Right)", text_colour, desired_image_size.width);
    
    // Draw the 3 depth images in a row
    const cv::Rect realsense_rgb_depth_area(
        realsense_rgb_image_area.tl().x + 1,
        realsense_rgb_image_area.br().y,
        desired_image_size.width,
        desired_image_size.height
    );
    if (!_realsense_rgb_depth_image.empty())
        _realsense_rgb_depth_image.copyTo( _ui_canvas(realsense_rgb_depth_area) );
    else
        _ui_canvas(realsense_rgb_depth_area).setTo(cv::Scalar(0, 0, 0));
    
    const cv::Rect realsense_infra1_depth_area(
        realsense_rgb_depth_area.tl().x + desired_image_size.width,
        realsense_rgb_depth_area.tl().y,
        desired_image_size.width,
        desired_image_size.height
    );
    if (!_realsense_infra1_depth_image.empty())
        _realsense_infra1_depth_image.copyTo( _ui_canvas(realsense_infra1_depth_area) );
    else
        _ui_canvas(realsense_infra1_depth_area).setTo(cv::Scalar(0, 0, 0));
    
    const cv::Rect realsense_infra2_depth_area(
        realsense_infra1_depth_area.tl().x + desired_image_size.width,
        realsense_infra1_depth_area.tl().y,
        desired_image_size.width,
        desired_image_size.height
    );
    if (!_realsense_infra2_depth_image.empty())
        _realsense_infra2_depth_image.copyTo( _ui_canvas(realsense_infra2_depth_area) );
    else
        _ui_canvas(realsense_infra2_depth_area).setTo(cv::Scalar(0, 0, 0));
    
    // Draw the log box
    cv::Rect log_box_area(
        endoscope_right_area.tl().x,
        endoscope_right_area.br().y + gutter_size,
        log_block_width,
        log_block_height
    );
    _ui_canvas(log_box_area).setTo(block_background);
    
    cv::Point text_coord;
    text_coord = log_box_area.tl();
    text_coord.x += padding.left;
    text_coord.y += padding.top + line_height;
    
    for (const auto& line: _log_lines) {
        put_text(text_coord, line.first, to_colour(line.second));
        text_coord.y += line_height + line_skip;
    }
    
    
    // Draw the usage box
    cv::Rect usage_box_area(
        log_box_area.br().x + gutter_size,
        log_box_area.tl().y,
        canvas_width - log_box_area.br().x - gutter_size,
        log_block_height
    );
    _ui_canvas( usage_box_area ).setTo(block_background);
    
    text_coord = usage_box_area.tl();
    text_coord.x += padding.left;
    text_coord.y += padding.top + line_height;
    
    for (const auto& line: _usage_lines) {
        put_text(text_coord, line, text_colour);
        text_coord.y += line_height + line_skip;
    }
    
    return _ui_canvas;
}


void CaptureUI::process_image(const sensor_msgs::ImageConstPtr& src, cv::Mat& dest) {
    
    cv::Mat source_image;
    if (src->encoding == "8UC1" or src->encoding == "16UC1") {
        source_image = cv_bridge::toCvShare(src)->image;
        cv::cvtColor(source_image, source_image, cv::COLOR_GRAY2BGR);
    } else {
        source_image = cv_bridge::toCvShare(src, sensor_msgs::image_encodings::BGR8)->image;
    }
    
    if (source_image.size() == desired_image_size) {
        dest = source_image.clone();
    } else {
        cv::resize(source_image, dest, desired_image_size);
    }
}



cv::Rect CaptureUI::put_text(
    const cv::Point2i& pos,
    const std::string& text,
    const cv::Scalar& colour,
    const float font_scale
) {
    const auto font_size = base_font_size * font_scale;
    const auto text_size = cv::getTextSize(text, font_face, font_scale, 1, nullptr);
    
    cv::putText(
        /* img = */ _ui_canvas,
        /* text = */ text,
        /* org = */ pos,
        /* fontFace = */ font_face,
        /* fontScale = */ font_size,
        /* color = */ colour,
        /* thickness = */ 1,
        /* lineType = */ cv::LINE_AA
    );
    
    return cv::Rect(pos, text_size);
}


cv::Rect CaptureUI::put_text_centred(
    cv::Point2i pos,
    const std::string& text,
    const cv::Scalar& colour,
    const int container_width,
    const float font_scale
) {
    const auto font_size = base_font_size * font_scale;
    const auto text_size = cv::getTextSize(text, font_face, font_scale, 1, nullptr);
    
    // Adjust the offset to ensure that the text is centred
    pos.x += (container_width - text_size.width) / 2;
    
    return put_text(pos, text, colour, font_scale);
}


cv::Scalar CaptureUI::to_colour(const MessageType type) {
    switch (type) {
        case MessageType::Success: return success_colour;
        case MessageType::Error:   return error_colour;
        default:                   return text_colour;
    }
};
