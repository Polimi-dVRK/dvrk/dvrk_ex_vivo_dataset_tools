//
// Created by tibo on 16/04/18.
//
// This program is a mess. I'm sorry. It works, that's all I can say to justify myself
//

#include <ros/ros.h>
#include <std_msgs/Empty.h>
#include <cv_bridge/cv_bridge.h>
#include <dynamic_reconfigure/client.h>
#include <sensor_msgs/image_encodings.h>
#include <realsense2_camera/rs435_paramsConfig.h>


#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl_ros/point_cloud.h>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <dvrk_common/opencv/highgui.hpp>
#include <dvrk_common/ros/camera/simple_camera.hpp>
#include <dvrk_common/ros/camera/stereo_camera.hpp>

#include <dvrk_camera_calibration/util.hpp>
#include <dvrk_camera_calibration/camera_params.hpp>

#include "ex_vivo_organ_dataset_capture_ui.hpp"

/*
 *                      CONFIGURATION VARIABLES
 */

const std::string opencv_window_name = "dVRK Organ Dataset Acquisition";
const std::string realsense_depth_emitter_enable_param =
    "/realsense/realsense2_camera_manager/rs435_depth_emitter_enabled";

/*
 *                      GLOBAL STATE VARIABLES
 */

fs::path dataset_folder_name;
unsigned int dataset_size = 1;

CaptureUI ui;

enum class AcquisitionType {
    Endoscope, Realsense
};
AcquisitionType acquisition_type = AcquisitionType::Endoscope;

using realsense_dyn_reconfigure = dynamic_reconfigure::Client<realsense2_camera::rs435_paramsConfig>;
boost::shared_ptr<realsense_dyn_reconfigure> realsense_params;

sensor_msgs::ImageConstPtr g_endoscope_left = nullptr;
sensor_msgs::ImageConstPtr g_endoscope_right = nullptr;
image_geometry::PinholeCameraModel g_endoscope_left_camera_model;
image_geometry::PinholeCameraModel g_endoscope_right_camera_model;

sensor_msgs::ImageConstPtr g_realsense_rgb = nullptr;
sensor_msgs::ImageConstPtr g_realsense_infra1 = nullptr;
sensor_msgs::ImageConstPtr g_realsense_infra2 = nullptr;
sensor_msgs::ImageConstPtr g_realsense_rgb_depth = nullptr;
sensor_msgs::ImageConstPtr g_realsense_infra1_depth = nullptr;
sensor_msgs::ImageConstPtr g_realsense_infra2_depth = nullptr;
image_geometry::PinholeCameraModel g_realsense_infra1_camera_model;
image_geometry::PinholeCameraModel g_realsense_infra2_camera_model;
image_geometry::PinholeCameraModel g_realsense_rgb_camera_model;

typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud_t;
PointCloud_t::ConstPtr g_realsense_cloud = nullptr;

// This publisher is used to signal the motion node to continue with its motion after we have saved
// the images
ros::Publisher continue_motion_pub;

ros::Timer save_image_event_timer;

/*
 *                      Forward Declarations
 */

void save_current_snapshot();

/*
 *                      ROS TOPIC CALLBACKS
 */

void on_endoscope_images(
    const sensor_msgs::ImageConstPtr& left,
    const sensor_msgs::ImageConstPtr& right,
    const image_geometry::StereoCameraModel& camera_model
) {
    g_endoscope_left = left;
    g_endoscope_right = right;
    g_endoscope_left_camera_model = camera_model.left();
    g_endoscope_right_camera_model = camera_model.right();
}

void on_realsense_infra_images(
    const sensor_msgs::ImageConstPtr& left,
    const sensor_msgs::ImageConstPtr& right,
    const image_geometry::StereoCameraModel& camera_model
) {
    g_realsense_infra1 = left;
    g_realsense_infra2 = right;
    g_realsense_infra1_camera_model = camera_model.right();
    g_realsense_infra2_camera_model = camera_model.left();
}

void on_realsense_infra_depth_images(
    const sensor_msgs::ImageConstPtr& left,
    const sensor_msgs::ImageConstPtr& right,
    const image_geometry::StereoCameraModel& /* unused */
) {
    g_realsense_infra1_depth = left;
    g_realsense_infra2_depth = right;

}

void on_realsense_rgb_image(
    const sensor_msgs::ImageConstPtr& image,
    const image_geometry::PinholeCameraModel&  camera_model
) {
    g_realsense_rgb = image;
    g_realsense_rgb_camera_model = camera_model;
}

void on_realsense_rgb_depth_image(
    const sensor_msgs::ImageConstPtr& image,
    const image_geometry::PinholeCameraModel&  /* unused */
) {
    g_realsense_rgb_depth = image;
}

void on_realsense_pointcloud(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr& cloud) {
    g_realsense_cloud = cloud;
}

void save_current_snapshot_wrapper(const ros::TimerEvent&) {
    save_current_snapshot();
}

void on_save_images_event(ros::NodeHandle& nh, const std_msgs::EmptyConstPtr) {
    ui.add_log("Got <save_image> notification from ECM controller ...", MessageType::Info);
    
    const ros::Duration delay(1.0);
    save_image_event_timer = nh.createTimer(delay, save_current_snapshot_wrapper, true);
}

bool enable_realsense_emitter(bool enable) {
    // Off (disabled) -> 0, On (enabled) -> 1
    int parameter_value = (enable)? 1 : 0;
    
    realsense2_camera::rs435_paramsConfig cfg;
    cfg.rs435_depth_emitter_enabled = parameter_value;
    if (!realsense_params->setConfiguration(cfg)) {
        ROS_ERROR("Unable to set realsense camera configuration");
        return false;
    }
    
    return true;
}

/*
 *                      INTERESTING BITS
 */

bool save_image(const sensor_msgs::ImageConstPtr& image_ptr, fs::path filename) {
    cv::Mat source_image;
    if (!image_ptr) {
        ROS_WARN("Image to be saved to %s is empty. Cannot continue", filename.string().c_str());
        return false;
    }
    
    if (image_ptr->encoding == "8UC1" or image_ptr->encoding == "16UC1") {
        source_image = cv_bridge::toCvShare(image_ptr)->image;
        cv::cvtColor(source_image, source_image, cv::COLOR_GRAY2BGR);
    } else if (sensor_msgs::image_encodings::isColor(image_ptr->encoding)) {
        source_image = cv_bridge::toCvShare(image_ptr, sensor_msgs::image_encodings::BGR8)->image;
    } else {
        source_image = cv_bridge::toCvShare(image_ptr, sensor_msgs::image_encodings::MONO8)->image;
    }
    
    return cv::imwrite(filename.string(), source_image);
}

void save_current_snapshot() {
    // First off we create a new folder in which to put the results:

    std::string acquisition_type_str;
    switch(acquisition_type) {
        case AcquisitionType::Endoscope:
            acquisition_type_str = "endoscope";
            break;
        case AcquisitionType::Realsense:
            acquisition_type_str = "realsense";
            break;

    }

    if (dataset_folder_name.empty()) {
        dataset_folder_name = fs::current_path() /
            (dvrk::time_str_now("%Y-%m-%d-%H-%M-%S") + "-dvrk-organ-series-" + acquisition_type_str);
        if (!fs::exists(dataset_folder_name) and !fs::create_directory(dataset_folder_name)) {
            ui.add_log("Unable to create directory " + dataset_folder_name.string(), MessageType::Error);
            return;
        }
        
        switch (acquisition_type) {
            case AcquisitionType::Endoscope: {
                auto endoscope_left_params =
                    dvrk::CameraParams::FromPinholeCameraModel(g_endoscope_left_camera_model);
                endoscope_left_params.to_file(dataset_folder_name / "endoscope_left_camera_params.yml");
    
                const auto endoscope_right_params =
                    dvrk::CameraParams::FromPinholeCameraModel(g_endoscope_right_camera_model);
                endoscope_left_params.to_file(dataset_folder_name / "endoscope_right_camera_params.yml");
                
                break;
            }
            
            case AcquisitionType::Realsense: {
                const auto realsense_infra1_params =
                    dvrk::CameraParams::FromPinholeCameraModel(g_realsense_infra1_camera_model);
                realsense_infra1_params.to_file(dataset_folder_name / "realsense_infra1_camera_params.yml");
    
                const auto realsense_infra2_params =
                    dvrk::CameraParams::FromPinholeCameraModel(g_realsense_infra2_camera_model);
                realsense_infra2_params.to_file(dataset_folder_name / "realsense_infra2_camera_params.yml");
    
                const auto realsense_rgb_params =
                    dvrk::CameraParams::FromPinholeCameraModel(g_realsense_rgb_camera_model);
                realsense_rgb_params.to_file(dataset_folder_name / "realsense_rgb_camera_params.yml");
                
                break;
            }
            
            default: break;
        }
        
        
    }
    
    /*
     * The acquisition happens in several phases:
     *   - First the endoscope images
     *   - Then the realsense images and depths 
     */
    
    fs::path sample_subfolder = dataset_folder_name / (std::string("acquisition_") + std::to_string(dataset_size));
    if (!fs::create_directory(sample_subfolder)) {
         ui.add_log(std::string("Unable to create smple subfolder ") + sample_subfolder.string(), MessageType::Error);
         return;
    }
    
    ui.add_log("Saving sample to " + sample_subfolder.string(), MessageType::Info);
    
    switch (acquisition_type) {
        case AcquisitionType::Endoscope: {
            if (!save_image(g_endoscope_left, sample_subfolder / "endoscope_left.png")) {
                ui.add_log(
                    "Unable to save image " + (sample_subfolder / "endoscope_left.png").string(),
                    MessageType::Error
                );
            }
    
            if (!save_image(g_endoscope_right, sample_subfolder / "endoscope_right.png")) {
                ui.add_log(
                    "Unable to save image " + (sample_subfolder / "endoscope_right.png").string(),
                    MessageType::Error
                );
            }
            
            break;
        }
    
        case AcquisitionType::Realsense: {
            if (g_realsense_cloud) {
                pcl::io::savePCDFileBinaryCompressed(
                    (sample_subfolder / "realsense_pointcloud.pcd").string(), *g_realsense_cloud);
            } else {
                ui.add_log("Unable to save pointcloud. Is the point cloud actively published ?", MessageType::Error);
            }
    
            if (!save_image(g_realsense_rgb_depth, sample_subfolder / "realsense_rgb_depth.png")) {
                ui.add_log(
                    "Unable to save image " + (sample_subfolder / "realsense_rgb_depth.png").string(),
                    MessageType::Error
                );
            }
    
            if (!save_image(g_realsense_infra1_depth, sample_subfolder / "realsense_infra1_depth.png")) {
                ui.add_log("Unable to save image "
                    + (sample_subfolder / "realsense_infra1_depth.png").string(), MessageType::Error
                );
            }
    
    
            if (!save_image(g_realsense_infra2_depth, sample_subfolder / "realsense_infra2_depth.png")) {
                ui.add_log("Unable to save image "
                    + (sample_subfolder / "realsense_infra2_depth.png").string(), MessageType::Error
                );
            }
            
            enable_realsense_emitter(false);

            ros::Time sleep_end = ros::Time::now() + ros::Duration(.25);
            while (ros::Time::now() < sleep_end) {
                ros::spinOnce();
                ros::Duration(.01).sleep();
            }
            
            if (!save_image(g_realsense_rgb, sample_subfolder / "realsense_rgb.png")) {
                ui.add_log("Unable to save image " + (sample_subfolder / "realsense_rgb.png").string(),
                    MessageType::Error
                );
            }
    
            if (!save_image(g_realsense_infra1, sample_subfolder / "realsense_infra1.png")) {
                ui.add_log(
                    "Unable to save image " + (sample_subfolder / "realsense_infra1.png").string(),
                    MessageType::Error
                );
            }
    
            if (!save_image(g_realsense_infra2, sample_subfolder / "realsense_infra2.png")) {
                ui.add_log(
                    "Unable to save image " + (sample_subfolder / "realsense_infra2.png").string(),
                    MessageType::Error
                );
            }
            
            enable_realsense_emitter(true);
        }
        
    }
    
    ++dataset_size;
    ROS_INFO("Sample complete, notifying ecm controller ...");
    continue_motion_pub.publish(std_msgs::Empty());
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "organ_dataset_capture");
    
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");
    
    // ros::AsyncSpinner spinner(1);
    
    std::string acquisition_type_str;
    if (!pnh.getParam("acquisition_type", acquisition_type_str)) {
        ROS_ERROR("Acquisition type parameter not set. Please set the `acquisition_type` parameter to one of 'endoscope', realsense_image', 'realsenese_cloud'");
        return -1;
    }
    
    if (acquisition_type_str == "endoscope") {
        acquisition_type = AcquisitionType::Endoscope;
    } else if (acquisition_type_str == "realsense") {
        acquisition_type = AcquisitionType::Realsense;
    } else {
        ROS_ERROR("Acquisition type parameter set to an invalid value. Please set the `acquisition_type` parameter to one of 'endoscope', realsense_image', 'realsenese_cloud'");
        return -1;
    }
    
    continue_motion_pub = nh.advertise<std_msgs::Empty>("acquisition_completed", 1);
    
    auto save_images_cb = boost::bind(on_save_images_event, nh, _1);
    ros::Subscriber save_image_event = nh.subscribe<std_msgs::Empty>("motion_completed", 1, save_images_cb);
    
    dvrk::StereoCamera endoscope_camera(
        nh, "endoscope/left/image_raw", "endoscope/right/image_raw", on_endoscope_images);
    
    dvrk::StereoCamera realsense_infra_camera(
        nh, "realsense/infra1/image_rect_raw", "realsense/infra2/image_rect_raw", on_realsense_infra_images);
    dvrk::StereoCamera realsense_infra_depth_camera(
        nh, "realsense/aligned_depth_to_infra1/image_raw", "realsense/aligned_depth_to_infra2/image_raw", on_realsense_infra_depth_images);
    
    dvrk::SimpleCamera realsense_rgb_camera(
        nh, "realsense/color/image_raw", on_realsense_rgb_image);
    dvrk::SimpleCamera realsense_rgb_depth_camera(
        nh, "realsense/aligned_depth_to_color/image_raw", on_realsense_rgb_depth_image);
    
    ros::Subscriber realsense_cloud_sub = nh.subscribe<pcl::PointCloud<pcl::PointXYZRGB>>(
        "/realsense/depth_registered/points",1 , on_realsense_pointcloud);
    
    cv::namedWindow(opencv_window_name, cv::WINDOW_NORMAL);
    ui.add_log("dVRK ex-vivo dataset acquisition node ready. Press <S> to save a new sample", MessageType::Info);
    
    realsense_params = boost::make_shared<realsense_dyn_reconfigure>("/realsense/realsense2_camera_manager/");
    
    enable_realsense_emitter(true);
    bool realsense_emitter_enabled = true;
    
    ros::Rate loop_rate(24);
    while (ros::ok()) {
        ros::spinOnce();
        
        if (g_endoscope_left)         ui.set_endoscope_left_image(g_endoscope_left);
        if (g_endoscope_right)        ui.set_endoscope_right_image(g_endoscope_right);
        if (g_realsense_infra1)       ui.set_realsense_infra1_image(g_realsense_infra1);
        if (g_realsense_infra1_depth) ui.set_realsense_infra1_depth_image(g_realsense_infra1_depth);
        if (g_realsense_infra2)       ui.set_realsense_infra2_image(g_realsense_infra2);
        if (g_realsense_infra2_depth) ui.set_realsense_infra2_depth_image(g_realsense_infra2_depth);
        if (g_realsense_rgb)          ui.set_realsense_rgb_image(g_realsense_rgb);
        if (g_realsense_rgb_depth)    ui.set_realsense_rgb_depth_image(g_realsense_rgb_depth);
        
        cv::imshow(opencv_window_name, ui.render());
        
        dvrk::KeyCode keycode = dvrk::waitKey(1);
        switch(keycode) {
            case dvrk::KeyCode::Q:
            case dvrk::KeyCode::Escape:
                ros::requestShutdown();
                break;
                
            case dvrk::KeyCode::S:
                save_current_snapshot();
                break;
                
            case dvrk::KeyCode::F:
                dvrk::toggleFullScreen(opencv_window_name);
                break;
                
            case dvrk::KeyCode::E:
                realsense_emitter_enabled = !realsense_emitter_enabled;
                if (!enable_realsense_emitter(realsense_emitter_enabled)) {
                    ROS_ERROR("Unable to toggle laser state");
                }
                ui.add_log("Toggled realsense IR emitter", MessageType::Info);
                break;
                
            default:
                break;
        }
        
        loop_rate.sleep();
    }
    
    ROS_INFO("Shutting down cleanly");
    return 0;
}
