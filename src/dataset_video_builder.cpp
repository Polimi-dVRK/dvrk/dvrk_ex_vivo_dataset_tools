//
// Created by tibo on 23/04/18.
//

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <ros/ros.h>
#include <dvrk_common/opencv/util.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>

const cv::Size desired_image_size(640, 480);
const cv::Size final_frame_size(
    desired_image_size.width * 2,
    desired_image_size.height * 2
);

void show_usage() {

}

cv::Mat read_image(fs::path image_path) {
    if (fs::exists(image_path) and fs::is_regular_file(image_path)) {
        cv::Mat original = cv::imread(image_path.string());
        if (original.empty()) {
            std::cerr << "Unable to read " << image_path << ".\n";
            return cv::Mat(desired_image_size, CV_8UC3, dvrk::Colour::LightCyan);
        }
        
        if (original.size() != desired_image_size) {
            cv::Mat destination;
            cv::resize(original, destination, desired_image_size);
            original = destination;
        }
        
        if (original.channels() != 3) {
            std::cerr << "Expected 3 channel image. Image " << image_path << " only has "
                      << original.channels() << " channels.";
            return cv::Mat(desired_image_size, CV_8UC3, dvrk::Colour::LightCyan);
        }
        
        return original;
        
    } else {
        std::cerr << "Image " << image_path << " is missing or not a file.\n";
        return cv::Mat(desired_image_size, CV_8UC3, dvrk::Colour::LightCyan);
    }
}

int main(int argc, char** argv) {
    
    if (argc != 3) {
        std::cerr << "Error: invalid number of arguments.\n";
        show_usage();
        return -1;
    }
    
    fs::path input_folder = fs::absolute(argv[1]);
    if (!fs::exists(input_folder) and not fs::is_directory(input_folder)) {
        std::cerr << "Error: input folder " << input_folder << " is not a directory.\n";
        return -1;
    }
    
    fs::path output_viddeo = fs::absolute(argv[2]);
    if (fs::exists(output_viddeo)) {
        std::cerr << "Error: output file " << output_viddeo << " already exist.\n";
        return -1;
    }
    
    cv::VideoWriter video_writer(
        /* filename = */ output_viddeo.string(),
        /* fourcc = */cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), // Video encoding
        /* fps = */ 24,
        /* frameSize = */ final_frame_size
    );
    if (!video_writer.isOpened()) {
        std::cerr << "Unable to create video writer\n";
        return -1;
    }
    
    // We expect the input folder to contain a number of other directories named acquistion_X
    unsigned int acquisition_count = 0;
    while (true) {
        ++acquisition_count;
        
        fs::path acquisition_folder =
            input_folder / (std::string("acquisition_") + std::to_string(acquisition_count));
        if (!fs::exists(acquisition_folder)) {
            break;
        }
        
        // Assemble video frame
        cv::Mat final_frame(final_frame_size, CV_8UC3, cv::Scalar(0, 0, 0));
        
        fs::path endoscope_left = acquisition_folder / "endoscope_left.png";
        cv::Mat endoscope_left_image = read_image(endoscope_left);
        cv::Rect endoscope_left_roi(
            0, 0, desired_image_size.width, desired_image_size.height);
        endoscope_left_image.copyTo(final_frame( endoscope_left_roi ));
        
        fs::path endoscope_right = acquisition_folder / "endoscope_right.png";
        cv::Mat endoscope_right_image = read_image(endoscope_right);
        cv::Rect endoscope_right_roi(
                desired_image_size.width, 0,
                desired_image_size.width, desired_image_size.height
        );
        endoscope_right_image.copyTo(final_frame( endoscope_right_roi ));
    
        fs::path realsense_color = acquisition_folder / "realsense_rgb.png";
        cv::Mat realsense_color_image = read_image(realsense_color);
        cv::Rect realsense_color_roi(
            0, desired_image_size.height,
            desired_image_size.width, desired_image_size.height
        );
        realsense_color_image.copyTo(final_frame( realsense_color_roi ));
        
        fs::path realsense_infra = acquisition_folder / "realsense_infra1.png";
        cv::Mat realsense_infra_image = read_image(realsense_infra);
        cv::Rect realsense_infra_roi(
            desired_image_size.width, desired_image_size.height,
            desired_image_size.width, desired_image_size.height
        );
        realsense_infra_image.copyTo(final_frame( realsense_infra_roi ));
        
        video_writer.write(final_frame);
        
    }
    
    video_writer.release();
    std::cout << "Video file saved to " << output_viddeo << "\n";
    return 0;
}
